/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.afilechooser.actions;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;
import com.ipaulpro.afilechooser.utils.FileUtils;

public class ON_FILE_CHOSEN extends ActionBase {
	public ON_FILE_CHOSEN() {
	}

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		boolean bReturn = true;
		super.initialize(ctx, v, pView, a, tpvl);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return Boolean.valueOf(false);
		}
		Intent data = tpvl.getParcelable("data");
		bReturn &= data != null;
		if (bReturn) {
			final Uri uri = data.getData();
			bReturn &= uri != null;
	
			try {
				// Create a file instance from the URI
				final File file = FileUtils.getFile(uri);
				Toast.makeText(appembleActivity.getContext(), 
						"File Selected: "+file.getAbsolutePath(), Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				Log.e("FileSelectorTestActivity", "File select error", e);
			}
		}		
		return executeCascadingActions(bReturn?Boolean.TRUE:Boolean.FALSE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}