/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.afilechooser.actions;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import com.appemble.afilechooser.utils.FileDialog;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;

public class FILE_DIALOG extends ActionBase {
	public FILE_DIALOG() {
	}

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return Boolean.FALSE;
		}
		Activity activity = null;
		if (appembleActivity instanceof Activity)
			activity = (Activity) appembleActivity;
		if (null == activity)
			return Boolean.FALSE;
        FileDialog fileDialog = new FileDialog(activity, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
//        fileDialog.setFileEndsWith(".zip");
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
            	Utilities.putData(targetParameterValueList, Constants.STRING, "file", file.toString());
        		executeCascadingActions(Boolean.TRUE, targetParameterValueList);
            }
        });
        fileDialog.showDialog();
		
		return Boolean.FALSE;
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}